def memobase_has_clockify_tag(task):
    if "project" in task and task["project"].startswith("memobase"):
        allowed_tags = ["ausbau", "betrieb", "support", "ignore"]
        assert "cyt" in task, "cyt tag (i.e. clockify tag value) needs to be defined in Memobase project!"
        assert task["cyt"] in allowed_tags, "Clockify tag '" + task["cyt"] + "' is not allowed. Must be one of '" + "', '".join(allowed_tags) + "'"

def task_has_allowed_tags(task):
    allowed_tags = ["answer", "design", "discuss", "document", "fix", "implement", "investigate", "plan", "prepare", "remind", "support", "next"]
    assert "tags" in task and task["tags"], "Task needs at least one tag. Allowed tags are '" + "', '".join(allowed_tags) + "'"
    for tag in task["tags"]:
        assert tag in allowed_tags, "Tag '" + tag + "' is not allowed. Must be one of '" + "', '".join(allowed_tags) + "'"
    

